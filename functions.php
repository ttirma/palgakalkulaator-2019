<?php

function calculateNetSalary($brutoPalk) {
    $tulumaksuvabaMiinimum = 500;

    $unemploymentPay = $brutoPalk * 0.016;
    $pension = $brutoPalk * 0.02;
    $maksustatavTulu = $brutoPalk - $tulumaksuvabaMiinimum - $unemploymentPay - $pension;

    $tulumaks = 0;

    if ($maksustatavTulu > 0) {
        $tulumaks = $maksustatavTulu * 0.2;
    }

    $netopalk = $brutoPalk - $unemploymentPay - $pension - $tulumaks;

    return ["töötuskindlustusMakse" => $unemploymentPay,
        "kogumispension" => $pension,
        "tulumaks" => $tulumaks,
        "netopalk" => $netopalk];
}

function calculateEmployerExpenses($brutoPalk) {
    $sotsiaalmaks = $brutoPalk * 0.33;
    $tooandjaTootuskindlustus = $brutoPalk * 0.008;
    $kogukulu = $brutoPalk + $tooandjaTootuskindlustus + $sotsiaalmaks;

    return ["sotsiaalmaks" => $sotsiaalmaks,
        "tööandja töötuskindlustus" => $tooandjaTootuskindlustus,
        "kogukulu" => $kogukulu];
}
