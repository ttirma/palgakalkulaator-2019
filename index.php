<?php
require_once("vendor/tpl.php");
require_once ("functions.php");

$cmd = "";
if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}


if ($cmd === "calculate") {
    $brutoPalk = $_POST["bruto"];
    $tootajaMaksud = calculateNetSalary($brutoPalk);
    $tooandjaKulud = calculateEmployerExpenses($brutoPalk);

    $data = [
      "subTemplate" => "result.html",
        "tootajaMaksud" => $tootajaMaksud,
        "tooandjaKulud" => $tooandjaKulud
    ];
    print(renderTemplate("templates/main.html", $data));
} else {
    $data = ["subTemplate" => "form.html"];
    print(renderTemplate("templates/main.html", $data));
}